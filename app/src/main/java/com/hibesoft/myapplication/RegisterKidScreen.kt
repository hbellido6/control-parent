package com.hibesoft.myapplication

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.hibesoft.myapplication.components.HeaderComponent

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterScreen(onNavigateToHome: () -> Unit) {
    Column {
        HeaderComponent()

        Column(modifier = Modifier.padding(30.dp)) {
            Card(
                colors = CardDefaults.cardColors(
                    containerColor = MaterialTheme.colorScheme.surfaceVariant,
                )
            ) {
                Column(
                    modifier = Modifier.padding(
                        top = 10.dp,
                        bottom = 30.dp,
                        start = 30.dp,
                        end = 30.dp
                    ),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    TextField(value = "", onValueChange = {}, label = { Text(text = "Codigo de Dispositivo") })
                    TextField(value = "", onValueChange = {}, label = { Text(text = "Nombre") })
                    TextField(value = "", onValueChange = {}, label = { Text(text = "Edad") })
                    TextField(value = "", onValueChange = {}, label = { Text(text = "Genero") })
                    TextField(value = "", onValueChange = {}, label = { Text(text = "Foto") })
                }
            }
            Button(
                onClick = {
                    onNavigateToHome()
                }, modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp)
            ) {
                Text(text = "REGISTRAR KID")
            }
        }
    }
}


