package com.hibesoft.myapplication

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.hibesoft.myapplication.components.HeaderComponent

@Composable
fun HomeScreen(onNavigateToApps: () -> Unit, onNavigateKidRegister: () -> Unit) {
    Column {
        HeaderComponent()
        Column(modifier = Modifier.padding(16.dp)) {
            Row(horizontalArrangement = Arrangement.SpaceEvenly) {
                CircularProfile("Juan", R.drawable.people, profileClick = { onNavigateToApps() })
                CircularProfile("Juan", R.drawable.gril, profileClick = { onNavigateToApps() })
                CircularAddButton {
                    onNavigateKidRegister()
                }
            }

        }
    }
}

@Composable
fun CircularAddButton(onClick: () -> Unit) {
    Column(modifier = Modifier.padding(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        FloatingActionButton(
            onClick = onClick, shape = CircleShape, modifier = Modifier
                .width(80.dp)
                .height(80.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_add),
                contentDescription = null,
                tint = Color.White
            )
        }

        Text(text = "nuevo Kid")
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CardOption(text: String, onNavigateToApps: () -> Unit) {
    Card(onClick = { onNavigateToApps() }, modifier = Modifier.padding(horizontal = 10.dp)) {
        Column(
            modifier = Modifier.padding(20.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Image(painter = painterResource(id = R.drawable.apps), contentDescription = "")

            Text(text = text)
        }
    }
}

@Composable
fun CircularProfile(name: String, imageResourceId: Int, profileClick: () -> Unit) {
    Column(modifier = Modifier.padding(5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        Button(
            onClick = { profileClick() },
            shape = CircleShape,
            modifier = Modifier
                .width(80.dp)
                .height(80.dp)
        ) {
            Image(painter = painterResource(id = imageResourceId), contentDescription = "")
        }
        Text(text = name)
    }
}
