package com.hibesoft.myapplication

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

@Composable
fun AppNavHost(
    navController: NavHostController = rememberNavController(), startDestination: String = "login"
) {
    NavHost(navController = navController, startDestination = startDestination) {
        composable(route = "login") {
            LoginScreen(onNavigateToHome = { navController.navigate("home") })
        }
        composable(route = "home") {
            HomeScreen(onNavigateToApps = { navController.navigate("apps") },
                onNavigateKidRegister = { navController.navigate("register") })
        }
        composable(route = "apps") {
            val context = navController.context
            Apps(context)
        }
        composable(route = "register") {
            val context = navController.context
            RegisterScreen(onNavigateToHome = {
                navController.navigate("home")
            })
        }

    }
}