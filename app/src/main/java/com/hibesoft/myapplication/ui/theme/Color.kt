package com.hibesoft.myapplication.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Green80 = Color(0xFFA3FF8C) // Verde al 80%
val GreenGrey80 = Color(0xFFC2DCCC) // Verde grisáceo al 80%
val Mint80 = Color(0xFFB8F7D4) // Menta al 80%

val DarkerGreen40 = Color(0xFF004d00) // Verde más oscuro al 40%
val GreenGrey40 = Color(0xFF689F38) // Verde grisáceo al 40%
val Mint40 = Color(0xFF8BC34A) // Menta al 40%