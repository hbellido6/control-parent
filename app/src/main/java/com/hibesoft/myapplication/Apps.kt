package com.hibesoft.myapplication

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.hibesoft.myapplication.components.HeaderComponent

@Composable
fun Apps(context: Context) {

    val installedApps by remember {
        val apps = getInstalledUserApps(context)
        mutableStateOf(apps)
    }

    val packageManager = context.packageManager
    var blockApps by remember { mutableStateOf(false) }

    val appNamesAndIcons by remember(installedApps) {
        mutableStateOf(installedApps.filter { it.enabled }.map { appInfo ->
            val appName = packageManager.getApplicationLabel(appInfo).toString()
            val appIcon = packageManager.getApplicationIcon(appInfo)
            Pair(appName, appIcon)
        })
    }

    Column {
        HeaderComponent()
        Row(modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp)) {
            Text(
                modifier = Modifier.weight(1f),
                text = "Lista de applicaciones",
                fontWeight = FontWeight(600)
            )
            Text(
                modifier = Modifier.padding(start = 16.dp),
                text = "bloqueado",
                fontWeight = FontWeight(600)
            )
        }
        LazyColumn(modifier = Modifier.padding(horizontal = 20.dp, vertical = 10.dp)) {
            items(appNamesAndIcons) { (appName, appIcon) ->
                Row(verticalAlignment = Alignment.CenterVertically) {
                    Icon(
                        painter = rememberAsyncImagePainter(appIcon),
                        contentDescription = appName,
                        modifier = Modifier.size(48.dp)
                    )
                    Text(appName, modifier = Modifier.weight(1f))
                    Icon(
                        painter = painterResource(id = R.drawable.time),
                        contentDescription = appName,
                        modifier = Modifier.size(30.dp)
                    )
                    Switch(
                        checked = blockApps, onCheckedChange = {
                            blockApps = it;
                        }, modifier = Modifier.padding(start = 16.dp)
                    )
                }
                Divider()
            }
        }
    }
}

private fun getInstalledUserApps(context: Context): List<ApplicationInfo> {
    val packageManager: PackageManager = context.packageManager
    val apps: List<ApplicationInfo> =
        packageManager.getInstalledApplications(PackageManager.GET_META_DATA)
    return apps.filter { it.flags and ApplicationInfo.FLAG_SYSTEM == 0 }
}