package com.hibesoft.myapplication

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun LoginScreen(onNavigateToHome: () -> Unit) {
    var isLogin by remember { mutableStateOf(true) }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(horizontal = 40.dp)
    ) {
        Title()
        Image(
            painter = painterResource(id = R.drawable.kids),
            contentDescription = "",
            modifier = Modifier.height(220.dp)
        )
        Box(modifier = Modifier.padding(10.dp)) {
            SwitchLoginRegister(changeIsLogin = {
                isLogin = it
            })
        }

        if (isLogin) {
            LoginForm(onNavigateToHome)
        }
        RegisterForm(changeIsLogin = {
            isLogin = it
        })

    }
}

@Composable
fun Title() {
    Text(
        modifier = Modifier.fillMaxWidth(),
        textAlign = TextAlign.Center,
        fontSize = 24.sp,
        text = "Family kids"
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginForm(onNavigateToHome: () -> Unit) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        )
    ) {
        Column(
            modifier = Modifier.padding(top = 10.dp, bottom = 30.dp, start = 30.dp, end = 30.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                text = "Bienvenidos a\nFamily kids",
                fontSize = 16.sp,
                fontWeight = FontWeight(600)
            )
            TextField(value = "", onValueChange = {}, label = { Text(text = "Correo") })
            TextField(value = "", onValueChange = {}, label = { Text(text = "Contraseña") })
        }
    }
    Button(
        onClick = {
            onNavigateToHome()
        }, modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 10.dp)
    ) {
        Text(text = "INICIA SESION")
    }
    Text(
        modifier = Modifier.fillMaxWidth(),
        textAlign = TextAlign.Center,
        text = "Olvidaste tu contraseña?"
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterForm(changeIsLogin: (Boolean) -> Unit) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        )
    ) {
        Column(
            modifier = Modifier.padding(top = 10.dp, bottom = 30.dp, start = 30.dp, end = 30.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                text = "Bienvenidos a Family kids",
                fontSize = 16.sp,
                fontWeight = FontWeight(600)
            )
            TextField(value = "", onValueChange = {}, label = { Text(text = "Nombre") })
            TextField(value = "", onValueChange = {}, label = { Text(text = "Correo") })
            TextField(value = "", onValueChange = {}, label = { Text(text = "Contraseña") })
            Row(verticalAlignment = Alignment.CenterVertically) {
                Checkbox(checked = false, onCheckedChange = {})
                Text(fontSize = 12.sp, text = "Aceptar terminos de privacidad")
            }
        }
    }
    Button(
        onClick = {
            changeIsLogin(false)
        }, modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 10.dp)
    ) {
        Text(text = "CREAR CUENTA")
    }
    Text(
        modifier = Modifier.fillMaxWidth(),
        textAlign = TextAlign.Center,
        text = "Olvidaste tu contraseña?"
    )
}


@Composable
fun SwitchLoginRegister(changeIsLogin: (Boolean) -> Unit) {
    var activeButton by remember { mutableStateOf(1) }
    Row(
        modifier = Modifier
            .clip(MaterialTheme.shapes.extraLarge)
            .height(40.dp)
            .background(MaterialTheme.colorScheme.surfaceVariant)
    ) {
        Button(
            onClick = {
                activeButton = 1
                changeIsLogin(true)
            }, colors = ButtonDefaults.buttonColors(
                containerColor = if (activeButton == 1) MaterialTheme.colorScheme.primary else Color.Transparent
            )
        ) {
            Text(text = "Inicia Sesion", color = Color.White)
        }

        Button(
            onClick = {
                activeButton = 2
                changeIsLogin(false)
            }, colors = ButtonDefaults.buttonColors(
                containerColor = if (activeButton == 2) MaterialTheme.colorScheme.primary else Color.Transparent
            )
        ) {
            Text(text = "Crear Cuenta", color = Color.White)
        }
    }
}